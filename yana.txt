
The name of Yanadaiah indicates you are a diligent and persevering worker who enjoys a routine occupation where you can do a job well and finish what you start.
 
You like to work at your own speed, without pressure, as you prefer to take your time to work step by step in your own way.
 
You could become frustrated and thwarted in your efforts if too many changes or disruptions occur.
 
Also, you do not appreciate people enforcing new methods or ideas on you, as you like to examine all the details before making changes.
 
It is difficult for you to be spontaneous and affectionate with those close to you, as suitable words and actions do not come to mind quickly.
 
In association your tendency to state your mind simply and clearly, without diplomacy or finesse, can lead to awkwardness or embarrassment.
 
Your fondness for excess quantities of heavy, starchy foods could cause stomach or intestinal disorders; you could also suffer with head tension, affecting the eyes, ears, sinus, throat, or loss of hair